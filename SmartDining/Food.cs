﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartDining
{
    class Food
    {        
        public List<string> allergens = new List<string>();
        public string InName;
        public double InPrice;    

        public Food(string Name, double Price)
        {
            InName = Name;
            InPrice = Price; 
        }
        public override string ToString()
        {
            return InName + "\n" + "Dkk " + InPrice;
        }
        public void addAllergen(string myIngredient)
        {
            allergens.Add(myIngredient);
        }

    }
}
