﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartDining
{
    class Program
    {
        static void Main(string[] args)
        {
            Program myProgram = new Program();
            myProgram.Run();
        }
        public List<Restaurant> restaurants = new List<Restaurant>();
        public List<Food> allergenFreeFood = new List<Food>();
        public List<User> users = new List<User>();
        public List<Restaurant> chosenRestaurants = new List<Restaurant>();
        public List<string> chosenAllergens = new List<string>();
        string chosenRealName = "";
        int chosenTime = ' ';
        double totalSum = ' ';
        string ChosenWhereToEat = "";
        string myUserName = "";
        int personsAtTable = 0;
        private void Run()
        {
            LoadRestaurantsAndMeals();

            SignInMenu();
        }
        private void LoadRestaurantsAndMeals()
        {
            Restaurant wokkaBox = new Restaurant("Wokkabox", "Vestre Stationsvej 42, 5000 Odense C", 65384359, 1530, 2300, 7.5, "asian");
            Restaurant umashi = new Restaurant("Umashi", "Nørregade 24, 5000 Odense C", 64183725, 1130, 2300, 6.5, "asian");
            Restaurant barSushi = new Restaurant("Bar Sushi", "Læssøegade 1, 5000 Odense C", 66136300, 1045, 2200, 6.0, "asian");
            Restaurant chinaWokHouse = new Restaurant("China Wokhouse", "Overgade 25, 5000 Odense C", 64896564, 1630, 2100, 6.5, "asian");
            Restaurant daIsabella = new Restaurant("Da Isabella", "Vindegade 67, 5000 Odense C", 66125266, 1230, 2100, 5.5, "italian");
            Restaurant tonys = new Restaurant("Tonys", "Vindegade 12, 5000 Odense c", 65123514, 1700, 2200, 5.5, "italian");
            Restaurant theItalian = new Restaurant("The Italian", "Nørregade 15, 5000 Odense C", 64457898, 1300, 2100, 6.5, "italian");
            Restaurant eraOra = new Restaurant("Era Ora", "Toldbodsgade 11, 5000 Odense C", 65184956, 1700, 2200, 4.5, "italian");
            Restaurant bergama = new Restaurant("Bergama", "Vesterbro 11, 5000 Odense", 65914001, 1200, 2200, 6.5, "turkish");
            Restaurant sultanPalace = new Restaurant("Sultan Palace", "Georgsgade 15, 5000 Odense C", 64152302, 1200, 0300, 6.5, "turkish");
            Restaurant ankara = new Restaurant("Restaurant Ankara", "Vestergade 23, Odense C", 63152432, 1530, 2300, 7.5, "turkish");
            Restaurant özKonyaKebab = new Restaurant("Öz Konya Kebab", "Kongensgade 34, 5000 Odense C", 65123545, 1130, 0500, 4.5, "turkish");
            Restaurant denGrimmeÆlling = new Restaurant("Den Grimme Ælling", "Hans Jensens Stræde 1, 5000 Odense C", 65917030, 1600, 2200, 7.0, "danish");
            Restaurant brasserieSkovbakken = new Restaurant("Brasserie Skovbakken", "Rugårdsvej 187, 5210 Odense NV", 65875268, 1730, 2230, 7.5, "danish");
            Restaurant kokOgVin = new Restaurant("Kok & Vin", "Ternevej 2, 5210 Odense NV", 66748967, 1430, 2300, 6.5, "danish");
            Restaurant olufBagersGaard = new Restaurant("Restaurant Oluf Bagers Gaard", "Thomas B. Thriges Gade 14, 5000 Odense C", 65891572, 1000, 2200, 9.5, "danish");
            Restaurant esPåSkovriderkroen = new Restaurant("Restaurant ES på Skovriderkroen", "Duppegade 22, 5000 Odense C", 60131213, 1030, 2000, 6.0, "danish");
            Restaurant elTorito = new Restaurant("El Torito", "Vestergade 21, 5000 Odense C", 66130202, 1600, 2200, 4.5, "spanish");
            Restaurant mesonEspana = new Restaurant("Mesón España", "Falen 45, 5000 Odense C", 65847899, 1330, 2200, 5.5, "spanish");
            Restaurant casetasEspana = new Restaurant("Casetas España", "Vesterbro 77, 5000 Odense C", 88779955, 1630, 2400, 8.8, "spanish");

            restaurants.Add(wokkaBox);
            restaurants.Add(umashi);
            restaurants.Add(barSushi);
            restaurants.Add(chinaWokHouse);
            restaurants.Add(daIsabella);
            restaurants.Add(tonys);
            restaurants.Add(theItalian);
            restaurants.Add(eraOra);
            restaurants.Add(bergama);
            restaurants.Add(sultanPalace);
            restaurants.Add(ankara);
            restaurants.Add(özKonyaKebab);
            restaurants.Add(denGrimmeÆlling);
            restaurants.Add(brasserieSkovbakken);
            restaurants.Add(kokOgVin);
            restaurants.Add(olufBagersGaard);
            restaurants.Add(esPåSkovriderkroen);
            restaurants.Add(elTorito);
            restaurants.Add(mesonEspana);
            restaurants.Add(casetasEspana);

            //Asiatiske retter
            Food newFood = new Food("Sushi solo", 85.00);
            barSushi.addFood(newFood);
            newFood = new Food("Sushi Duo", 155.00);
            barSushi.addFood(newFood);
            newFood = new Food("Sushi Big", 235.00);
            barSushi.addFood(newFood);
            newFood = new Food("Wokret", 99.00);
            barSushi.addFood(newFood);
            string myString = "Soja";
            newFood.addAllergen(myString);
            myString = "Gluten";
            newFood.addAllergen(myString);

            newFood = new Food("Ret 1", 85.00);
            wokkaBox.addFood(newFood);
            newFood = new Food("Ret 2", 155.00);
            wokkaBox.addFood(newFood);
            newFood = new Food("Ret 3", 235.00);
            wokkaBox.addFood(newFood);

            newFood = new Food("Ret 1", 85.00);
            umashi.addFood(newFood);
            newFood = new Food("Ret 2", 155.00);
            umashi.addFood(newFood);
            newFood = new Food("Ret 3", 235.00);
            umashi.addFood(newFood);

            newFood = new Food("Ret 1", 85.00);
            chinaWokHouse.addFood(newFood);
            newFood = new Food("Ret 2", 155.00);
            chinaWokHouse.addFood(newFood);
            newFood = new Food("Ret 3", 235.00);
            chinaWokHouse.addFood(newFood);


            // Italienske retter
            newFood = new Food("Spaghetti Carbonara", 75.00); // Retten
            daIsabella.addFood(newFood);                      // Restauranten som retten tilføjes til
            myString = "Gluten";                              // Allergener i retten
            newFood.addAllergen(myString);
            myString = "Æg";
            newFood.addAllergen(myString);
            myString = "Laktose";
            newFood.addAllergen(myString);
            newFood = new Food("Rejecocktail", 59.00);        // Ny ret
            daIsabella.addFood(newFood);
            myString = "Laktose";
            newFood.addAllergen(myString);
            myString = "Skaldyr";
            newFood.addAllergen(myString);
            newFood = new Food("Tiramisu", 50.00);
            daIsabella.addFood(newFood);
            myString = "Gluten";
            newFood.addAllergen(myString);
            myString = "Laktose";
            newFood.addAllergen(myString);
            myString = "Æg";
            newFood.addAllergen(myString);
            newFood = new Food("Bruschetta", 50.00);
            daIsabella.addFood(newFood);
            myString = "Nødder";
            newFood.addAllergen(myString);
            myString = "Laktose";
            newFood.addAllergen(myString);
            myString = "Gluten";
            newFood.addAllergen(myString);
            newFood = new Food("Pizza Margherita", 69.00);
            tonys.addFood(newFood);
            myString = "Laktose";
            newFood.addAllergen(myString);
            myString = "Gluten";
            newFood.addAllergen(myString);
            newFood = new Food("Pizza Salami", 79.00);
            tonys.addFood(newFood);
            myString = "Laktose";
            newFood.addAllergen(myString);
            myString = "Gluten";
            newFood.addAllergen(myString);
            newFood = new Food("Pizza Hawaii", 79.00);
            tonys.addFood(newFood);
            myString = "Laktose";
            newFood.addAllergen(myString);
            myString = "Gluten";
            newFood.addAllergen(myString);
            newFood = new Food("Entrecôte alla Grillia", 179.00);
            eraOra.addFood(newFood);
            newFood = new Food("Scallopine al Tartufo", 169.00);
            eraOra.addFood(newFood);
            newFood = new Food("Salmone alla Grillia", 155.00);
            eraOra.addFood(newFood);
            newFood = new Food("Ravioli", 79.00);
            theItalian.addFood(newFood);
            newFood = new Food("Grillet oksemørbrad", 149.00);
            theItalian.addFood(newFood);
            newFood = new Food("Lasagne", 89.00);
            theItalian.addFood(newFood);
            myString = "Laktose";
            newFood.addAllergen(myString);
            myString = "Gluten";
            newFood.addAllergen(myString);

            //Tyrkiske retter
            newFood = new Food("Cacik m. brød", 25.00);
            bergama.addFood(newFood);
            myString = "Laktose";
            newFood.addAllergen(myString);
            myString = "Gluten";
            newFood.addAllergen(myString);
            newFood = new Food("Fyldte vinblade", 65.00);
            bergama.addFood(newFood);
            myString = "Æg";
            newFood.addAllergen(myString);
            newFood = new Food("Lammekød m .ris", 75.00);
            bergama.addFood(newFood);
            newFood = new Food("Oksekød m .ris", 75.00);
            bergama.addFood(newFood);
            newFood = new Food("Pandekage m. is", 55.00);
            bergama.addFood(newFood);
            myString = "Laktose";
            newFood.addAllergen(myString);
            myString = "Gluten";
            newFood.addAllergen(myString);

            newFood = new Food("Ret 1", 85.00);
            sultanPalace.addFood(newFood);
            newFood = new Food("Ret 2", 155.00);
            sultanPalace.addFood(newFood);
            newFood = new Food("Ret 3", 235.00);
            sultanPalace.addFood(newFood);

            newFood = new Food("Ret 1", 85.00);
            ankara.addFood(newFood);
            newFood = new Food("Ret 2", 155.00);
            ankara.addFood(newFood);
            newFood = new Food("Ret 3", 235.00);
            ankara.addFood(newFood);

            newFood = new Food("Ret 1", 85.00);
            özKonyaKebab.addFood(newFood);
            newFood = new Food("Ret 2", 155.00);
            özKonyaKebab.addFood(newFood);
            newFood = new Food("Ret 3", 235.00);
            özKonyaKebab.addFood(newFood);

            // Danske retter         
            newFood = new Food("Tarteletter", 65.00);
            denGrimmeÆlling.addFood(newFood);
            myString = "Æg";
            newFood.addAllergen(myString);
            myString = "Gluten";
            newFood.addAllergen(myString);
            newFood = new Food("Stegt Flæsk m. persillesovs", 80.00);
            denGrimmeÆlling.addFood(newFood);
            myString = "Laktose";
            newFood.addAllergen(myString);
            newFood = new Food("Stjerneskud ", 89.00);
            denGrimmeÆlling.addFood(newFood);
            myString = "Laktose";
            newFood.addAllergen(myString);
            myString = "Gluten";
            newFood.addAllergen(myString);
            newFood = new Food("Ribbensteg", 79.00);
            denGrimmeÆlling.addFood(newFood);
            newFood = new Food("Dansk hjemmelavet flødeis", 65.00);
            denGrimmeÆlling.addFood(newFood);
            myString = "Laktose";
            newFood.addAllergen(myString);
            myString = "Æg";
            newFood.addAllergen(myString);
            newFood = new Food("Koldskål", 45);
            denGrimmeÆlling.addFood(newFood);
            myString = "Æg";
            newFood.addAllergen(myString);
            myString = "Gluten";
            newFood.addAllergen(myString);
            myString = "Laktose";
            newFood.addAllergen(myString);


            newFood = new Food("Ret 1", 85.00);
            brasserieSkovbakken.addFood(newFood);
            newFood = new Food("Ret 2", 155.00);
            brasserieSkovbakken.addFood(newFood);
            newFood = new Food("Ret 3", 235.00);
            brasserieSkovbakken.addFood(newFood);

            newFood = new Food("Ret 1", 85.00);
            kokOgVin.addFood(newFood);
            newFood = new Food("Ret 2", 155.00);
            kokOgVin.addFood(newFood);
            newFood = new Food("Ret 3", 235.00);
            kokOgVin.addFood(newFood);

            newFood = new Food("Ret 1", 85.00);
            olufBagersGaard.addFood(newFood);
            newFood = new Food("Ret 2", 155.00);
            olufBagersGaard.addFood(newFood);
            newFood = new Food("Ret 3", 235.00);
            olufBagersGaard.addFood(newFood);

            newFood = new Food("Ret 1", 85.00);
            esPåSkovriderkroen.addFood(newFood);
            newFood = new Food("Ret 2", 155.00);
            esPåSkovriderkroen.addFood(newFood);
            newFood = new Food("Ret 3", 235.00);
            esPåSkovriderkroen.addFood(newFood);

            //Spanske retter  
            newFood = new Food("Avocadosuppe m. brød", 55.00);
            elTorito.addFood(newFood);
            myString = "Laktose";
            newFood.addAllergen(myString);
            myString = "Gluten";
            newFood.addAllergen(myString);
            newFood = new Food("Hvidløgsbrød", 20.00);
            elTorito.addFood(newFood);
            myString = "Laktose";
            newFood.addAllergen(myString);
            myString = "Gluten";
            newFood.addAllergen(myString);
            newFood = new Food("Burrito m. ris", 75.00);
            elTorito.addFood(newFood);
            myString = "Laktose";
            newFood.addAllergen(myString);
            myString = "Gluten";
            newFood.addAllergen(myString);
            newFood = new Food("Chili Con Carne m. ris", 70.00);
            elTorito.addFood(newFood);
            newFood = new Food("Bananasplit", 55.00);
            elTorito.addFood(newFood);
            myString = "Laktose";
            newFood.addAllergen(myString);

            newFood = new Food("Ret 1", 85.00);
            mesonEspana.addFood(newFood);
            newFood = new Food("Ret 2", 155.00);
            mesonEspana.addFood(newFood);
            newFood = new Food("Ret 3", 235.00);
            mesonEspana.addFood(newFood);

            newFood = new Food("Ret 1", 85.00);
            casetasEspana.addFood(newFood);
            newFood = new Food("Ret 2", 155.00);
            casetasEspana.addFood(newFood);
            newFood = new Food("Ret 3", 235.00);
        }
        public void SignInMenu()
        {
            User myUser = new User("julie", "mig", "Julie Sindhu Jørgensen", 29930647, "sindhu_89@msn.com");
            users.Add(myUser);

            bool keepRunning = true;
            while (keepRunning)
            {
                Console.Clear();
                int input = ' ';
                Console.WriteLine("Velkommen til SmartDining");
                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine("0. Afslut program");
                Console.WriteLine("1. Opret en bruger");
                Console.WriteLine("2. Login");

                try
                {
                    input = int.Parse(Console.ReadLine());
                }
                catch (FormatException)
                {
                    Console.WriteLine("Forkert input, indtast venligst et tal");
                    input = 10;
                    System.Threading.Thread.Sleep(2500);
                }

                switch (input)
                {
                    case 0:
                        keepRunning = false;
                        break;
                    case 1:
                        CreateUser();
                        break;
                    case 2:
                        SignInExistingUser();
                        break;
                    case 10:
                        break;
                    default:
                        SignInMenu();
                        break;
                }
            }
        }
        private void SignInExistingUser()
        {
            string InputUserName = " ";
            string InputUserPassword = " ";
            bool UserFound = false;
            Console.Clear();
            Console.Write("Brugernavn:");
            InputUserName = Console.ReadLine();
            Console.Write("Password:");
            InputUserPassword = Console.ReadLine();

            foreach (User user in users)
            {
                if (InputUserName == user.InUserName && InputUserPassword == user.InUserPassword)
                {
                    chosenRealName = user.InRealName;
                    myUserName = user.InUserName;
                    UserFound = true;
                }

                if (!UserFound)
                {
                    Console.Clear();
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Brugeren blev ikke fundet, tjek om brugernavn eller password er forkert");
                    Console.WriteLine("Vent venligst mens vi dirigerer dig tilbage til oprettelse");
                    Console.ResetColor();
                    System.Threading.Thread.Sleep(3500);
                }
            }
            if (UserFound)
            {
                OverviewMenu();
            }
        }
        public void CreateUser()
        {
            string userName = " ";
            string userPassword = " ";
            string checkPassword = "";
            string realName = " ";
            int phoneNumber = ' ';
            string email = " ";

            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Velkommen til SmartDining!\nOpret venligst en bruger herunder");
            Console.WriteLine();
            Console.ResetColor();
            Console.Write("Indtast et brugernavn:");
            userName = Console.ReadLine();
            foreach (User user in users)
            {
                if (user.InUserName == userName)
                {
                    Console.WriteLine("Dette brugernavn eksisterer allerede, vælg venligst et andet brugernavn");
                    System.Threading.Thread.Sleep(2000);
                    CreateUser();
                }
                else
                {
                    Console.Write("Indtast et password: ");
                    userPassword = Console.ReadLine();
                    Console.Write("Bekræft password: ");
                    checkPassword = Console.ReadLine();
                    if (userPassword == checkPassword)
                    {
                        Console.Write("Indtast fulde navn: ");
                        realName = Console.ReadLine();
                        Console.Write("Indtast et telefon nummer: ");
                        phoneNumber = int.Parse(Console.ReadLine());
                        Console.Write("Indtast en gyldig email adresse: ");
                        email = Console.ReadLine();
                        User newUser = new User(userName, userPassword, realName, phoneNumber, email);
                        chosenRealName = realName;
                        myUserName = userName;
                        users.Add(newUser);
                        OverviewMenu();
                    }
                    else
                    {
                        Console.WriteLine("Dette password stemmer ikke overens med det valgte password, start oprettelse igen");
                        System.Threading.Thread.Sleep(2000);
                        CreateUser();
                    }
                }
            }
        }
        private void OverviewMenu()
        {
            Console.Clear();
            int input = ' ';
            Console.WriteLine("Velkommen til SmartDining " + "\n" + chosenRealName);
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("1. Vælg restaurant ud fra kategori");
            Console.WriteLine("2. Brugerprofil");
            Console.WriteLine("3. Vis liste over restauranter der bruger SmartDining");
            Console.WriteLine("4. Se tilbud");
            Console.WriteLine("5. Anmeld Restaurant");
            Console.WriteLine("6. Se restauranters nyeste reviews");
            Console.WriteLine("7. Se restauranters åbningstider");
            Console.WriteLine("8. Logud");
            try
            {
                input = int.Parse(Console.ReadLine());
            }
            catch (FormatException)
            {
                Console.WriteLine("Forkert input, indtast venligst et tal");
                input = 10;
                System.Threading.Thread.Sleep(2000);
                OverviewMenu();
            }
            switch (input)
            {

                case 1:
                    SelectRestaurantCategory();
                    break;
                case 2:
                    UserInfo();
                    break;
                case 3:
                    AllRestaurants();
                    break;
                case 4:
                    Offers();
                    break;
                case 5:
                    GiveReview();
                    break;
                case 6:
                    PrintReview();
                    break;
                case 7:
                    PrintOpeningHours();
                    break;
                case 8:
                    SignInMenu();
                    break;
                default:
                    OverviewMenu();
                    break;
            }
        }
        private void SelectRestaurantCategory()
        {
            Console.Clear();
            int input = ' ';
            Console.Clear();
            Console.WriteLine("Restauranter: ");
            Console.WriteLine("");
            Console.WriteLine("0. Tilbage");
            Console.WriteLine("1. Italienske restauranter");
            Console.WriteLine("2. Spanske restauranter");
            Console.WriteLine("3. Danske restauranter");
            Console.WriteLine("4. Tyrkiske restauranter");
            Console.WriteLine("5. Asiatiske restauranter");
            try
            {
                input = int.Parse(Console.ReadLine());
            }
            catch (FormatException)
            {
                Console.WriteLine("Forkert input, indtast et tal");
                input = 10;
                System.Threading.Thread.Sleep(2000);
                SelectRestaurantCategory();
            }
            switch (input)
            {
                case 0:
                    OverviewMenu();
                    break;
                case 1:
                    ShowRestaurantsFromChosenCategory("italian");
                    break;
                case 2:
                    ShowRestaurantsFromChosenCategory("spanish");
                    break;
                case 3:
                    ShowRestaurantsFromChosenCategory("danish");
                    break;
                case 4:
                    ShowRestaurantsFromChosenCategory("turkish");
                    break;
                case 5:
                    ShowRestaurantsFromChosenCategory("asian");
                    break;
                case 10:
                    break;
                default:
                    SelectRestaurantCategory();
                    break;
            }
        }
        private void ShowRestaurantsFromChosenCategory(string type)
        {
            Console.Clear();
            allergenFreeFood.Clear();
            foreach (User user in users)
            {
                if (user.InUserName == myUserName)
                {
                    user.usersFoods.Clear();
                    totalSum = 0;
                    chosenAllergens.Clear();
                    totalSum = 0;
                }
            }
            int count = 0;
            chosenRestaurants.Clear();
            int choice = ' ';
            Console.WriteLine("Vælg Restaurant:\n");
            Console.WriteLine("0. Tilbage");
            foreach (Restaurant restaurant in restaurants)
                if (restaurant.InType == type)
                {
                    count += 1;
                    Console.WriteLine("{0}.{1}", count, restaurant);
                    chosenRestaurants.Add(restaurant);
                }
            try
            {
                choice = int.Parse(Console.ReadLine());
            }
            catch (FormatException)
            {
                Console.WriteLine("Forkert input, indtast venligst et tal");
                System.Threading.Thread.Sleep(2500);
                ShowRestaurantsFromChosenCategory(type);
            }

            if (choice == 0)
            {
                SelectRestaurantCategory();
            }
            else
            {
                try
                {
                    Restaurant(chosenRestaurants[choice - 1]);
                }
                catch (ArgumentOutOfRangeException)
                {
                    Console.WriteLine("Indtast venligst et gyldigt tal");
                    System.Threading.Thread.Sleep(2500);
                    ShowRestaurantsFromChosenCategory(type);
                }
            }
            Console.ReadLine();
        }
        private void Restaurant(Restaurant myRestaurant)
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Velkommen til " + myRestaurant);
            Console.WriteLine();
            Console.ResetColor();
            int userChoice = ' ';
            Console.WriteLine("0. Tilbage til liste over restaurantkategorier");
            Console.WriteLine("1. Tilbage til liste over alle restauranter der bruger SmartDining");
            Console.WriteLine("2. Allergi sortering af menuen");
            Console.WriteLine("3. Vis menu");
            try
            {
                userChoice = int.Parse(Console.ReadLine());
            }
            catch (FormatException)
            {
                Console.WriteLine("Forkert input, indtast venligst et tal");
                System.Threading.Thread.Sleep(2500);
                Restaurant(myRestaurant);
            }

            switch (userChoice)
            {
                case 0:
                    SelectRestaurantCategory();
                    break;
                case 1:
                    AllRestaurants();
                    break;
                case 2:
                    ChooseAllergens(myRestaurant);
                    break;
                case 3:
                    NormalMenu(myRestaurant);
                    break;
                default:
                    Restaurant(myRestaurant);
                    break;
            }
        }
        private void AllRestaurants()
        {
            Console.Clear();
            int userInput = ' ';
            allergenFreeFood.Clear();
            foreach (User user in users)
            {
                if (user.InUserName == myUserName)
                {
                    user.usersFoods.Clear();
                    chosenAllergens.Clear();
                    totalSum = 0;
                }
            }
            Console.WriteLine("Restauranter der bruger SmartDining");
            Console.WriteLine();
            Console.WriteLine("0. Tilbage");
            int count = 1;
            foreach (Restaurant restaurant in restaurants)
            {
                Console.Write(count + ". ");
                Console.WriteLine(restaurant);
                count++;
            }
            try
            {
                userInput = int.Parse(Console.ReadLine());
            }
            catch (FormatException)
            {
                Console.WriteLine("Forkert input, indtast venligst et tal");
                System.Threading.Thread.Sleep(2500);
                AllRestaurants();
            }
            Console.Clear();

            if (userInput == 0)
            {
                OverviewMenu();
            }
            else
            {
                try
                {
                    Restaurant(restaurants[userInput - 1]);
                }
                catch (ArgumentOutOfRangeException)
                {
                    Console.WriteLine("Indtast venligst et gyldigt tal");
                    System.Threading.Thread.Sleep(2500);
                    AllRestaurants();
                }
            }
            Console.ReadLine();
        }
        private void ChooseAllergens(Restaurant myRestaurant)
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Cyan;
            int userChoice = ' ';
            Console.WriteLine("Vælg det allergen din mad ikke må indeholde");
            Console.WriteLine();
            Console.ResetColor();
            Console.WriteLine("0. Tilbage");
            Console.WriteLine("1. Æg");
            Console.WriteLine("2. Laktose");
            Console.WriteLine("3. Gluten");
            Console.WriteLine("4. Soja");
            Console.WriteLine("5. Skaldyr");
            Console.WriteLine("6. Nødder");
            Console.WriteLine("7. Slet gemte allergener");
            try
            {
                userChoice = int.Parse(Console.ReadLine());
            }
            catch (FormatException)
            {
                Console.WriteLine("Forkert input, indtast venligst et tal");
                System.Threading.Thread.Sleep(2500);
                AllRestaurants();
            }

            Console.Clear();

            switch (userChoice)
            {
                case 0:
                    Restaurant(myRestaurant);
                    break;
                case 1:
                    chosenAllergens.Add("Æg");
                    AllergenFreeMenu(chosenAllergens, myRestaurant);
                    break;
                case 2:
                    chosenAllergens.Add("Laktose");
                    AllergenFreeMenu(chosenAllergens, myRestaurant);
                    break;
                case 3:
                    chosenAllergens.Add("Gluten");
                    AllergenFreeMenu(chosenAllergens, myRestaurant);
                    break;
                case 4:
                    chosenAllergens.Add("Soja");
                    AllergenFreeMenu(chosenAllergens, myRestaurant);
                    break;
                case 5:
                    chosenAllergens.Add("Skaldyr");
                    AllergenFreeMenu(chosenAllergens, myRestaurant);
                    break;
                case 6:
                    chosenAllergens.Add("Nødder");
                    AllergenFreeMenu(chosenAllergens, myRestaurant);
                    break;
                case 7:
                    chosenAllergens.Clear();
                    ChooseAllergens(myRestaurant);
                    break;
                default:
                    Restaurant(myRestaurant);
                    break;
            }
        }
        private void AllergenFreeMenu(List<string> chosenAllergens, Restaurant myRestaurant)
        {
            Console.Clear();
            allergenFreeFood.Clear();
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Retter uden allergener");
            Console.ResetColor();
            Console.WriteLine();
            int count = 0;
            int userInput = ' ';
            int userInput2 = ' ';
            Console.WriteLine("0. Tilføj flere allergener");
            foreach (Food food in myRestaurant.foods)
            {
                bool foundAllergen = false;
                foreach (string s in food.allergens)
                {
                    if (chosenAllergens.Contains(s))
                    {
                        foundAllergen = true;
                    }

                }

                if (!foundAllergen)
                {
                    count++;
                    Console.WriteLine("{0}.{1}", count, food);
                    if (!allergenFreeFood.Contains(food))
                    {
                        allergenFreeFood.Add(food);
                    }

                }

            }
            if (count == 0)
            {
                Console.WriteLine("Der findes ingen retter uden allergener");

            }
            else
            {
                Console.WriteLine("7.Randomize et måltid");
            }


            try
            {
                userInput = int.Parse(Console.ReadLine());

            }
            catch (FormatException)
            {
                Console.WriteLine("Forkert input, indtast venligst et tal");
                System.Threading.Thread.Sleep(2500);
                foreach (User user in users)
                {
                    if (user.InUserName == myUserName)
                    {
                        user.usersFoods.Clear();
                        totalSum = 0;
                        AllergenFreeMenu(chosenAllergens, myRestaurant);
                    }
                }
            }

            if (userInput == 0)
            {
                ChooseAllergens(myRestaurant);
            }
            else if (userInput == 7)
            {
                RandomizeAllergenFreeMeal(myRestaurant);
            }
            else
            {
                foreach (User user in users)
                {
                    if (user.InUserName == myUserName)
                    {
                        try
                        {
                            user.AddToOrder(allergenFreeFood[userInput - 1]);
                            TotalSum(allergenFreeFood[userInput - 1]);
                        }
                        catch (ArgumentOutOfRangeException)
                        {
                            Console.WriteLine("Indtast venligst et gyldigt tal");
                            System.Threading.Thread.Sleep(2500);
                            user.usersFoods.Clear();
                            totalSum = 0;
                            AllergenFreeMenu(chosenAllergens, myRestaurant);
                        }

                        PrintUserOrder();
                        Console.WriteLine("Ønsker du at tilføje flere ting til din ordre? \n1. Ja  \n2.nej");
                        try
                        {
                            userInput2 = int.Parse(Console.ReadLine());
                        }
                        catch (FormatException)
                        {
                            Console.WriteLine("Forkert input, indtast venligst et tal");
                            System.Threading.Thread.Sleep(2500);
                            user.usersFoods.Clear();
                            totalSum = 0;
                            AllergenFreeMenu(chosenAllergens, myRestaurant);
                        }
                        if (userInput2 == 1)
                        {
                            AllergenFreeMenu(chosenAllergens, myRestaurant);
                        }
                        else
                        {
                            HandleOrderDetails(myRestaurant);
                        }
                    }
                }
            }
        }

        public void RandomizeAllergenFreeMeal(Restaurant myRestaurant)
        {
            Console.Clear();
            int order = ' ';
            int x = allergenFreeFood.Count();
            Random randomizer = new Random((int)DateTime.Now.Millisecond);
            int i = randomizer.Next(0, x);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Randomizeren har valgt: " + allergenFreeFood[i]);
            Console.ResetColor();
            Console.WriteLine(" Vil du tilføje dette til din ordre? \n1. ja \n2.nej");
            try
            {
                order = int.Parse(Console.ReadLine());

            }
            catch (FormatException)
            {
                Console.WriteLine("Forkert input, indtast venligst et tal");
                System.Threading.Thread.Sleep(2500);
                RandomizeAllergenFreeMeal(myRestaurant);
            }
            if (order == 1)
            {
                foreach (User user in users)
                {
                    if (user.InUserName == myUserName)
                    {
                        user.AddToOrder(allergenFreeFood[i]);
                        TotalSum(myRestaurant.foods[i]);
                    }
                }
            }

            else if (order == 2)
            {
                Console.WriteLine("Vil du 1. prøve at randomize igen? eller 2. returnere til menukortet");
                int userChoice = int.Parse(Console.ReadLine());
                if (userChoice == 1)
                {
                    RandomizeAllergenFreeMeal(myRestaurant);
                }

                else if (userChoice == 2)
                {
                    Restaurant(myRestaurant);
                }
            }
        }
        private void NormalMenu(Restaurant myRestaurant)
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Menukort");
            Console.WriteLine();
            Console.ResetColor();
            int count = 0;
            Console.WriteLine("0. Tilbage");
            int userInput = ' ';
            int userInput2 = ' ';

            foreach (Food food in myRestaurant.foods)
            {
                count += 1;
                Console.WriteLine("{0}.{1}", count, food);
            }
            Console.WriteLine("7. Vælg et tilfældigt måltid");


            try
            {
                userInput = int.Parse(Console.ReadLine());

            }
            catch (FormatException)
            {
                Console.WriteLine("Forkert input, indtast venligst et tal");
                System.Threading.Thread.Sleep(2500);
                foreach (User user in users)
                {
                    if (user.InUserName == myUserName)
                    {
                        user.usersFoods.Clear();
                        totalSum = 0;
                        AllergenFreeMenu(chosenAllergens, myRestaurant);
                    }
                }
            }

            if (userInput == 0)
            {
                Restaurant(myRestaurant);
            }
            else if (userInput == 7)
            {
                RandomizeAllergenFreeMeal(myRestaurant);
            }
            else
            {
                foreach (User user in users)
                {
                    if (user.InUserName == myUserName)
                    {
                        try
                        {
                            user.AddToOrder(myRestaurant.foods[userInput - 1]);
                            TotalSum(myRestaurant.foods[userInput - 1]);
                        }
                        catch (ArgumentOutOfRangeException)
                        {
                            Console.WriteLine("Indtast venligst et gyldigt tal");
                            System.Threading.Thread.Sleep(2500);
                            user.usersFoods.Clear();
                            totalSum = 0;
                            NormalMenu(myRestaurant);
                        }
                    }
                    PrintUserOrder();
                    Console.WriteLine("Ønsker du at tilføje flere ting til din ordre? \n1.Ja \n2.nej");
                    try
                    {
                        userInput2 = int.Parse(Console.ReadLine());
                    }
                    catch (FormatException)
                    {
                        Console.WriteLine("Forkert input, indtast venligst et tal");
                        System.Threading.Thread.Sleep(2500);
                        user.usersFoods.Clear();
                        totalSum = 0;
                        NormalMenu(myRestaurant);
                    }

                    if (userInput2 == 1)
                    {
                        NormalMenu(myRestaurant);
                    }

                    else
                    {
                        HandleOrderDetails(myRestaurant);
                    }

                }
            }
        }
        private void HandleOrderDetails(Restaurant myRestaurant)
        {
            foreach (User user in users)
            {
                if (user.InUserName == myUserName)
                {
                    Console.Clear();
                    int whereToEat = ' ';
                    int finalChoice = ' ';
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Indtast venligst tidspunktet du ønsker din mad klar på (Format 0000)");
                    Console.ResetColor();
                    try
                    {
                        chosenTime = int.Parse(Console.ReadLine());
                    }
                    catch (FormatException)
                    {
                        Console.WriteLine("Forkert input, indtast venligst et tal");
                        System.Threading.Thread.Sleep(2500);
                        HandleOrderDetails(myRestaurant);
                    }
                    if (chosenTime >= (myRestaurant.InOpeningHourStart + 15) && chosenTime < (myRestaurant.InOpeningHourEnd))
                    {
                        Console.Clear();
                        Console.WriteLine("Din mad er bestilt til kl " + chosenTime);
                        Console.Clear();
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("Vælg venligst om du vil have maden: 1.to stay/2.to go");
                        Console.ResetColor();
                        try
                        {
                            whereToEat = int.Parse(Console.ReadLine());
                        }
                        catch (FormatException)
                        {
                            Console.WriteLine("Forkert input, indtast venligst et tal");
                            System.Threading.Thread.Sleep(2500);
                            HandleOrderDetails(myRestaurant);
                        }

                        if (whereToEat == 1)
                        {
                            Console.Clear();
                            ChosenWhereToEat = "to stay";
                            Console.WriteLine("Du har valgt at spise på restauranten KL. " + chosenTime);
                            Console.WriteLine("Indtast venligst hvor mange personer du vil bestille bord til.");
                            try
                            {
                                personsAtTable = int.Parse(Console.ReadLine());
                                Console.WriteLine("Du har bestilt bord til: " + personsAtTable);
                            }
                            catch (FormatException)
                            {
                                Console.WriteLine("Forkert input, indtast venligst et tal");
                                System.Threading.Thread.Sleep(2500);
                                HandleOrderDetails(myRestaurant);
                            }
                        }
                        else if (whereToEat == 2)
                        {
                            Console.Clear();
                            ChosenWhereToEat = "to go";
                            Console.WriteLine("Din mad er bestilt som takeaway, maden er bestilt til kl " + chosenTime);
                        }
                        Console.Clear();
                        WriteOrderForConfirmation(user);
                        if (personsAtTable > 0 && ChosenWhereToEat == "to stay")
                        {
                            Console.Write("Valgt {0} kl {1} og har bestilt bord til {2}.\n1.Bekræft 2. Afbryd", ChosenWhereToEat, chosenTime, personsAtTable);
                        }
                        else
                        {
                            Console.Write("Valgt {0} kl {1}.\n1.Bekræft 2. Afbryd", ChosenWhereToEat, chosenTime);
                        }

                        try
                        {
                            finalChoice = int.Parse(Console.ReadLine());
                        }
                        catch (FormatException)
                        {
                            Console.WriteLine("Forkert input, indtast venligst et tal");
                            System.Threading.Thread.Sleep(2500);
                            HandleOrderDetails(myRestaurant);
                        }
                        if (finalChoice == 1)
                        {
                            WriteToFile(user);
                            Console.Clear();
                            Console.WriteLine("Tak for din ordre, den er nu sendt til restauranten, afvent bekræftelse fra " + myRestaurant.InName);
                            Console.WriteLine("Hvis du har problemer med din ordre kontakt venligst " + myRestaurant.InName + " på " + myRestaurant.InPhoneNumber);
                        }
                        else if (finalChoice == 2)
                        {

                            user.usersFoods.Clear();
                            totalSum = 0;
                            Restaurant(myRestaurant);
                        }
                        else
                        {
                            Console.WriteLine("Indtast venligst et gyldigt tal");
                            System.Threading.Thread.Sleep(2500);
                            HandleOrderDetails(myRestaurant);
                        }
                        Console.WriteLine("Tak for din ordre, den er nu sendt til restauranten, Vent venligst på bekræftigelse... Dette kan tage et par minutter");
                        System.Threading.Thread.Sleep(2000);
                        Console.WriteLine("...");
                        /*System.Threading.Thread.Sleep(2000);
                        Console.WriteLine("...");
                        System.Threading.Thread.Sleep(2000);
                        Console.WriteLine("...");
                        System.Threading.Thread.Sleep(2000);
                        Console.WriteLine("...");
                        System.Threading.Thread.Sleep(2000);*/
                        Console.WriteLine("...");
                        Console.Clear();
                        Console.WriteLine("Din ordre er nu bekræftet, du bliver nu dirigeret videre til betaling");
                        //Metode der fører til betaling
                        //Når betaling er gennemført sendes besked til restauranten om at gå i gang med ordren
                    }

                    else
                    {
                        Console.WriteLine("Du har prøvet at bestille maden til udenfor restaurantens åbningstid, bestil venligst inden for tidsrummet {0} til {1}", (myRestaurant.InOpeningHourStart + 15), (myRestaurant.InOpeningHourEnd));
                        System.Threading.Thread.Sleep(2500);
                        HandleOrderDetails(myRestaurant);
                    }

                    Console.ReadLine();
                }
            }
        }
        private void WriteOrderForConfirmation(User user)
        {
            Console.WriteLine("Du har bestilt:");
            foreach (Food food in user.usersFoods)
            {
                Console.WriteLine(food);
            }

        }
        public void RandomizeNormalMeal(Restaurant myRestaurant)
        {
            Console.Clear();
            int x = myRestaurant.foods.Count();
            Random randomizer = new Random((int)DateTime.Now.Millisecond);
            int i = randomizer.Next(0, x);
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Randomizeren har valgt: " + myRestaurant.foods[i]);
            Console.ResetColor();
            Console.WriteLine(" Vil du tilføje dette til din ordre? \n1. ja \n2.nej");
            int order = ' ';
            int userChoice = ' ';
            try
            {
                order = int.Parse(Console.ReadLine());
            }
            catch (FormatException)
            {
                Console.WriteLine("Forkert input, indtast venligst et tal");
                System.Threading.Thread.Sleep(2000);
                RandomizeNormalMeal(myRestaurant);
            }

            if (order == 1)
            {
                foreach (User user in users)
                {
                    if (user.InUserName == myUserName)
                    {
                        user.AddToOrder(myRestaurant.foods[i]);
                        TotalSum(myRestaurant.foods[i]);
                    }
                }
            }

            else if (order == 2)
            {
                Console.WriteLine("Vil du 1. prøve at randomize igen? eller 2. returnere til menukortet");
                try
                {
                    userChoice = int.Parse(Console.ReadLine());
                }
                catch (FormatException)
                {
                    Console.WriteLine("Forkert input, indtast venligst et tal");
                    System.Threading.Thread.Sleep(2500);
                    RandomizeNormalMeal(myRestaurant);
                }
                if (userChoice == 1)
                {
                    RandomizeNormalMeal(myRestaurant);
                }

                else if (userChoice == 2)
                {
                    Restaurant(myRestaurant);
                }
            }
        }
        private void UserInfo()
        {
            Console.Clear();
            Console.WriteLine("Dine informationer");
            Console.WriteLine();
            foreach (User user in users)
            {
                if (user.InUserName == myUserName)
                {
                    Console.WriteLine(user);
                }
            }
            Console.ReadLine();
            OverviewMenu();
        }
        private void Offers()
        {
            Console.Clear();
            Console.WriteLine("Der er ikke nogle tilbud lige nu. ");
            Console.ReadLine();
            OverviewMenu();
        }
        private void GiveReview()
        {
            string userRestaurant = "";
            double userReview = ' ';
            bool restaurantFound = false;
            string chosenRestaurant = "";
            Console.Clear();
            Console.WriteLine("0. Tilbage");
            Console.WriteLine();
            Console.WriteLine("Indtast navn på den restaurant du ønsker at anmelde: ");
            userRestaurant = Console.ReadLine();
            if (userRestaurant == "0")
            {
                OverviewMenu();
            }
            else
            {
                foreach (Restaurant restaurant in restaurants)
                {
                    if (restaurant.InName == userRestaurant)
                    {
                        restaurantFound = true;
                        chosenRestaurant = restaurant.InName;
                    }

                }
                if (restaurantFound)
                {
                    Console.WriteLine("Indtast venligst antal stjerner du ønsker at give restauranten");
                    try
                    {
                        userReview = double.Parse(Console.ReadLine());
                    }
                    catch (FormatException)
                    {
                        Console.WriteLine("Forkert input, indtast venligst et tal");
                        System.Threading.Thread.Sleep(2500);
                        GiveReview();
                    }
                    if (userReview >= 0 && userReview <= 10)
                    {
                        foreach (Restaurant restaurante in restaurants)
                        {
                            if (restaurante.InName == chosenRestaurant)
                            {
                                restaurante.InReview = userReview;
                                Console.WriteLine("Du har nu givet {0} {1} stjene(r). Vent venligst mens vi dirigerer dig til menuen.", restaurante, userReview);
                                System.Threading.Thread.Sleep(3000);
                            }
                        }

                    }
                    else
                    {
                        Console.WriteLine("Du skal indtaste et tal mellem 1 og 10");
                        System.Threading.Thread.Sleep(2500);
                        GiveReview();
                    }
                }

                else
                {
                    Console.WriteLine("Restauranten blev desværre ikke fundet, prøv igen");
                    System.Threading.Thread.Sleep(2500);
                    GiveReview();
                }

                OverviewMenu();
            }

        }
        private void PrintReview()
        {
            Console.Clear();
            int userInput = ' ';
            Console.WriteLine("Restauranternes nyeste anmeldelse");
            Console.WriteLine();
            Console.WriteLine("0. Tilbage");
            int count = 1;
            foreach (Restaurant restaurant in restaurants)
            {
                Console.Write("{0}. {1} - {2}", count, restaurant.InName, restaurant.InReview);
                Console.WriteLine();
                count++;
            }
            try
            {
                userInput = int.Parse(Console.ReadLine());
            }
            catch (FormatException)
            {
                Console.WriteLine("Forkert input, indtast venligst et tal");
                System.Threading.Thread.Sleep(2500);
                PrintReview();
            }
            if (userInput == 0)
            {
                OverviewMenu();
            }
            else
            {
                try
                {
                    Restaurant(restaurants[userInput - 1]);
                }
                catch (ArgumentOutOfRangeException)
                {
                    Console.WriteLine("Indtast venligst et gyldigt tal");
                    System.Threading.Thread.Sleep(2500);
                    PrintReview();
                }
            }
            Console.ReadLine();
        }
        private void PrintOpeningHours()
        {
            Console.Clear();
            int userInput = ' ';
            Console.WriteLine("Restauranternes åbningstider");
            Console.WriteLine();
            Console.WriteLine("0. Tilbage");
            int count = 1;
            foreach (Restaurant restaurant in restaurants)
            {
                Console.Write("{0}. {1} - {2} - {3}", count, restaurant.InName, restaurant.InOpeningHourStart, restaurant.InOpeningHourEnd);
                Console.WriteLine();
                count++;
            }
            try
            {
                userInput = int.Parse(Console.ReadLine());
            }
            catch (FormatException)
            {
                Console.WriteLine("Forkert input, indtast venligst et tal");
                System.Threading.Thread.Sleep(2500);
                PrintReview();
            }
            if (userInput == 0)
            {
                OverviewMenu();
            }
            else
            {
                try
                {
                    Restaurant(restaurants[userInput - 1]);
                }
                catch (ArgumentOutOfRangeException)
                {
                    Console.WriteLine("Indtast venligst et gyldigt tal");
                    System.Threading.Thread.Sleep(2500);
                    PrintReview();
                }
            }
            Console.ReadLine();
        }
        private void SignOut()
        {
            foreach (User user in users)
            {
                chosenRealName = "";
                chosenTime = ' ';
                totalSum = ' ';
                ChosenWhereToEat = "";
                myUserName = "";
                users.Clear();
                SignInMenu();
            }
        }
        private void PrintUserOrder()
        {
            foreach (User user in users)
            {
                if (user.InUserName == myUserName)
                {
                    Console.Clear();
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Din ordre er nu: ");
                    Console.ResetColor();
                    foreach (Food food in user.usersFoods)
                    {
                        Console.WriteLine(food);
                    }
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Total pris af din ordre: DKK " + totalSum);
                    Console.ResetColor();
                }
            }
        }
        public void WriteToFile(User myUser)
        {
            string filepath = Environment.ExpandEnvironmentVariables(@"C:\\Users\\%USERNAME%\\Desktop\\SmartDining");
            string path = Environment.ExpandEnvironmentVariables(@"C:\\Users\\%USERNAME%\\Desktop\\SmartDining\\Ordrer.txt");
            Environment.CurrentDirectory = Environment.ExpandEnvironmentVariables("C:\\Users\\%USERNAME%\\Desktop");
            if (!Directory.Exists(filepath))
            {
                Directory.CreateDirectory(filepath);
            }
            if (!File.Exists(path))
            {
                File.Create(@path).Dispose();
            }
            using (StreamWriter file = new StreamWriter(@path, true))
            {
                file.WriteLine("Bestillingstidspunkt: " + DateTime.Now);
                file.WriteLine();
                file.WriteLine("Kundens navn: " + myUser.InRealName);
                file.WriteLine();
                file.WriteLine("Bestilling: ");
                foreach (Food myFood in myUser.usersFoods)
                {
                    file.WriteLine(myFood + " ");
                }
                file.WriteLine();
                file.WriteLine("Ordrens total pris: DKK " + totalSum);
                file.WriteLine();
                file.WriteLine("Tid ordren er bestilt til: " + chosenTime);
                file.WriteLine();
                file.WriteLine("Kunden ønsker ordren: " + ChosenWhereToEat);
                file.WriteLine();
                if (ChosenWhereToEat == "to stay")
                {
                    file.WriteLine("Der ønskes bord til {0}", personsAtTable);
                    file.WriteLine();
                }
            }
        }
        public double TotalSum(Food food)
        {
            totalSum += food.InPrice;
            return totalSum;
        }
    }
}
