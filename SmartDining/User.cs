﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartDining
{
    class User
    {
        public string InUserName;        
        public string InUserPassword;
        public string InRealName;
        public int InPhoneNumber;
        public string InEmail;
        public List<Food> usersFoods = new List<Food>(); 

        public User(string userName, string userPassword, string realName, int phoneNumber, string email)
        {
            InUserName = userName;
            InUserPassword = userPassword;
            InRealName = realName; 
            InPhoneNumber = phoneNumber;
            InEmail = email; 
            
        }
        public void AddToOrder(Food usersFoodItem)
        {
            usersFoods.Add(usersFoodItem);
        }
        public override string ToString()
        {
            return "Username: " + InUserName + "\n" + "Password:" + InUserPassword + "\n" + "Name: " + InRealName + "\n" + "Phone number: " + InPhoneNumber + "\n" + "Email:" + InEmail;
        }
    }
}
