﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartDining
{
    class Restaurant
    {
        public List<Food> foods = new List<Food>();
        public string InName;
        public string InLocation;
        public int InOpeningHourStart;
        public int InOpeningHourEnd;
        public double InReview;
        public string InType;
        public int InPhoneNumber { get; set; }


        public Restaurant(string name, string location, int phoneNumber, int openingsHourStart, int openingHourEnd, double review, string type)
        {
            InName = name;
            InLocation = location;
            InPhoneNumber = phoneNumber; 
            InOpeningHourStart = openingsHourStart;
            InOpeningHourEnd = openingHourEnd;
            InReview = review;
            InType = type; 
        }

        public void addFood(Food myFood)
        {
            foods.Add(myFood);
        }

        public override string ToString()
        {
            return InName;
        }

        
    }
    

}
